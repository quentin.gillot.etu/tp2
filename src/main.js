import pizzaList from './pages/pizzaList.js';
import data from './data.js';
import Router from './Router.js';
const pizzaList1 = new pizzaList([]);
Router.titleElement = document.querySelector('.pageTitle');
Router.contentElement = document.querySelector('.pageContent');
Router.routes = [
	{path: '/', page: pizzaList1, title: 'La carte'}
];
Router.navigate('/');
pizzaList1.pizzas = data;
Router.navigate('/'); 
