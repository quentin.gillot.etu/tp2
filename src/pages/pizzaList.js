import Component from '../components/Component.js'
import PizzaThumbnail from '../components/pizzaThumbnail.js'
export default class pizzaList extends Component {
    #pizzas
    set pizzas(value){
        let list = [];
        this.#pizzas=value;
        for (let i = 0 ; i < this.#pizzas.length ; i++){
            list.push(new PizzaThumbnail (this.#pizzas[i]).render());
        }
        this.children = list;
    }
    constructor (pizzas){
        const list =[];
        for (let i =0 ; i < pizzas.length ; i++){
            list.push(new PizzaThumbnail(pizzas[i]));
        }      
        super ('section class="pizzaList"',null,list);
    }
}