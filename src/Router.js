import Component from "./components/Component.js";

export default class Router {
    static titleElement;
    static contentElement;
    static routes;
    
     static navigate (path){
    let chemin='';
    for (let i =0 ; i < this.routes.length ; i++){
        if (this.routes[i].path == path){
            chemin=this.routes[i];
        }
    }
       this.titleElement.innerHTML = new Component ('h1',null,chemin.title).render();
       this.contentElement.innerHTML = chemin.page.render();
    }
}

