
export default class Component {
	tagname;
	children;
	attribute;
	constructor(tagname,attribute,children){
		this.tagname=tagname;
		this.attribute=attribute;
		this.children= children;
	}
	render() {
		if (this.children == null || this.children == undefined){
			if (this.attribute !=null && this.attribute != undefined){
				return `<${this.tagname} ${this.attribute.name}= ${this.attribute.value} />`
			}
			return `<${this.tagname} />`
		}else {		

            if (this.children instanceof Array){
                let v =''; 
                for (let i = 0 ; i < this.children.length ; i++){
                    if (this.children[i] instanceof Component)
                        v+= this.children[i].render();    
                    else
                        v+=this.children [i];
				}		
				if (this.tagname == 'article')		
					return `<${this.tagname} class= "pizzaThumbnail"> ${v} </${this.tagname}>`;
				return `<${this.tagname} > ${v} </${this.tagname}>`
            }else{
            return `<${this.tagname}> ${this.children} </${this.tagname}>`;
            }
		}
    }
    
}